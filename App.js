import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './src/components/Login/LoginScreen';
import HomeScreen from './src/components/Home/HomeScreen';
import {AppContext} from './src/constants/context';

const Stack = createStackNavigator();

const App = () => {
  const [userExist, setUserExist] = useState(false);

  return (
    <>
      <NavigationContainer>
        <AppContext.Provider value={{setUserExist, infoText: 'Any text here'}}>
          <Stack.Navigator>
            {!userExist ? (
              <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{headerShown: false}}
              />
            ) : (
              <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{headerShown: false}}
              />
            )}
          </Stack.Navigator>
        </AppContext.Provider>
      </NavigationContainer>
    </>
  );
};

export default App;
