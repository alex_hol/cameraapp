import React, {useState} from 'react';
import {View, Text, TextInput, TouchableOpacity, Alert} from 'react-native';

import {users} from '../../data/users.json';
import {AppContext} from '../../constants/context';
import {styles} from './styles';

const LoginScreen = ({login}) => {
  const [email, setEmail] = useState('');
  const [pass, setPass] = useState('');

  const checkUserInfo = () => {
    const userExist = users.some((el) => {
      return el.email === email && el.password === pass;
    });
    if (userExist) {
      login(userExist);
    } else {
      Alert.alert(
        'Login failed',
        'Login or password incorrect',
        [{text: 'OK'}],
        {cancelable: false},
      );
    }
  };

  return (
    <>
      <View style={styles.container}>
        <View style={styles.inputView}>
          <TextInput
            style={styles.usernameInput}
            placeholder="Email"
            placeholderTextColor="#003f5c"
            value={email}
            onChangeText={(text) => setEmail(text)}
            autoCapitalize="none"
            keyboardType="email-address"
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.usernameInput}
            placeholder="Password"
            type="password"
            placeholderTextColor="#003f5c"
            value={pass}
            onChangeText={(text) => setPass(text)}
            autoCapitalize="none"
            secureTextEntry={true}
          />
        </View>
        <TouchableOpacity
          style={styles.loginBtn}
          onPress={() => checkUserInfo()}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    </>
  );
};

export default () => (
  <AppContext.Consumer>
    {({setUserExist}) => <LoginScreen login={setUserExist} />}
  </AppContext.Consumer>
);
