import React, {useState} from 'react';
import {TouchableOpacity, View, Text, StatusBar} from 'react-native';
import {RNCamera} from 'react-native-camera';
import CameraRoll from '@react-native-community/cameraroll';
import Icon from 'react-native-vector-icons/AntDesign';
import {createDrawerNavigator} from '@react-navigation/drawer';

import {AppContext} from '../../constants/context';
import {styles} from './styles';

const Drawer = createDrawerNavigator();

const HomeScreen = ({infoText}) => {
  const [cameraType, setCameraType] = useState('back');
  const [camera, setCamera] = useState(null);
  const [isRecording, setIsRecording] = useState(false);

  const changeCamera = async () => {
    if (camera) {
      await setCameraType((prev) => {
        return prev === 'back' ? 'front' : 'back';
      });
    }
  };

  const takePicture = async () => {
    if (camera) {
      const options = {quality: 1, base64: true};
      const data = await camera.takePictureAsync(options);
      await CameraRoll.save(data.uri);
    }
  };

  const takeVideo = async () => {
    if (camera) {
      try {
        const options = {quality: 1, fps: 60};
        const record = camera.recordAsync(options);

        if (record) {
          await setIsRecording(true);
          const data = await record;
          await setIsRecording(false);
          await CameraRoll.save(data.uri);
        }
      } catch (e) {
        console.error(e);
      }
    }
  };

  const stopRecording = async () => {
    if (camera) {
      try {
        if (isRecording) {
          await camera.stopRecording();
        }
      } catch (e) {
        console.error(e);
      }
    }
  };

  return (
    <View style={styles.wrapper}>
      <StatusBar hidden />
      <View style={styles.infoTextContainer}>
        <Text style={styles.infoText}>{infoText}</Text>
      </View>
      <RNCamera
        ref={(ref) => {
          setCamera(ref);
        }}
        style={styles.preview}
        type={cameraType}
        flashMode={RNCamera.Constants.FlashMode.off}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
      />
      <View style={styles.buttonsContainer}>
        <TouchableOpacity
          onPress={() => {
            isRecording ? stopRecording() : takePicture();
          }}
          onLongPress={() => {
            !isRecording ? takeVideo() : stopRecording();
          }}
          style={styles.capture}>
          <Icon
            name={isRecording ? 'videocamera' : 'camera'}
            size={32}
            color="#fff"
          />
        </TouchableOpacity>
        <TouchableOpacity onPress={changeCamera} style={styles.changeBtn}>
          <Icon name="swap" size={32} color="#fff" />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const HomeScreenConsumer = () => (
  <AppContext.Consumer>
    {(props) => <HomeScreen {...props} />}
  </AppContext.Consumer>
);

export default () => (
  <Drawer.Navigator>
    <Drawer.Screen name="Home" component={HomeScreenConsumer} />
  </Drawer.Navigator>
);
