import { Dimensions, StyleSheet } from "react-native";

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;

export const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'black',
  },
  preview: {
    flex: 0.85,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  capture: {
    borderRadius: 50,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    width: 70,
    height: 70,
    backgroundColor: '#00000070',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: width,
    flex: 0.15,
    backgroundColor: 'grey',
  },
  changeBtn: {
    borderRadius: 50,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
    margin: 20,
    width: 70,
    height: 70,
    backgroundColor: '#00000070',
  },
  infoTextContainer: {
    position: 'absolute',
    top: 30,
    left: 0,
    right: 0,
    bottom: 0,
    alignItems: 'center',
    zIndex: 1,
    height: height * 0.3,
  },
  infoText: {
    color: '#fff',
    width: width * 0.8,
    textAlign: 'center',
    fontSize: 22,
  },
});
